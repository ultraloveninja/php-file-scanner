# PHP File Scanner

This is a small PHP app that scans two directoires and then spits out the file path which will then append it to an array and output all of the HTML code.

I created this cause I was updating a site that someone had built before me. It was part of a gallery but the issue was that it required to manually update all of the paths in order for the gallery to work with the thumbnail images and the larger images. And then doing that for a number of pages was going to take FOREVER.

So, I created this just so I could get the HTML output and then copy that back into the gallery code.

It's probably not the greatest solution out there, but it worked for me and the project.
